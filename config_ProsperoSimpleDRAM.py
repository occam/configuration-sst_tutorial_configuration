import sst
import sys
import argparse
import os
#########################################
#		Parse commandline arguments		#
#########################################

parser = argparse.ArgumentParser()
parser.add_argument("--trace_prospero0", help="specify trace file")

args, unknown = parser.parse_known_args()

trace_file = args.trace_prospero0 if args.trace_prospero0!=None else "./dummy.trace"

## Look for trace file and create a dummy if it not found
print("Using trace %s"%(trace_file))
if( not os.path.exists(trace_file) ):
	f = open(trace_file, 'w+')
	f.write("0 r 0 0")
	f.close()

#########################
#		Configure		#
#########################
statFile = "stats.csv"
statLevel = 16


# Build Configuration Information
clock = "2660MHz"
memory_clock = "200MHz"
memory_capacity = 2048  #MB
cache_link_latency = "300ps"


# Connect Cores & caches
print "Configuring CPU..."
cpu = sst.Component("cpu", "prospero.prosperoCPU")
cpu.addParams({
	'verbose': 				0,
	'cache_line_size':		64,
	'reader':				'prospero.ProsperoTextTraceReader',
	'pagesize':				4096,
    'clock':				clock,
	'max_outstanding':		16,
	'max_issue_per_cycle':	2,
    'readerParams.file':	trace_file,
})

print "Configuring Memory"
memory = sst.Component("memory", "memHierarchy.MemController")
memory.addParams({
        "backend" : 			"memHierarchy.simpleDRAM",
        "backend.row_policy" : 	"open",
        "backend.mem_size" : 	memory_capacity,
        "clock" : 				memory_clock,
        "do_not_back" : 1
        })


cpu_memory_link = sst.Link("cpu_memory_link")
cpu_memory_link.connect((cpu, "cache_link", cache_link_latency), (memory, "direct_link", cache_link_latency))


# ===============================================================================

# Enable SST Statistics Outputs for this simulation
sst.setStatisticLoadLevel(statLevel)
sst.enableAllStatisticsForAllComponents({"type":"sst.AccumulatorStatistic"})

sst.setStatisticOutput("sst.statOutputCSV")
sst.setStatisticOutputOptions( {
    "filepath"  : statFile,
    "separator" : ", "
    } )

print "Completed configuring the EX1 model"
