import sst
import sys
import argparse
import os
#########################################
#		Parse commandline arguments		#
#########################################

parser = argparse.ArgumentParser()
parser.add_argument("--trace_prospero0", help="specify trace file")
parser.add_argument("--configuration_None_path", help="specify path for DRAMSIM2 configuration files")

args, unknown = parser.parse_known_args()

trace_file = args.trace_prospero0 if args.trace_prospero0!=None else "./dummy.trace"

dramsim_config_path = args.configuration_None_path if args.configuration_None_path!=None else "."

sys_ini = "%s/system.ini"%(dramsim_config_path)
dev_ini = "%s/device.ini"%(dramsim_config_path)

## Look for trace file and create a dummy if it not found
print("Using trace %s"%(trace_file))
print("Using dramsim:\n\t%s\n\t%s"%(sys_ini,dev_ini))
if( not os.path.exists(trace_file) ):
	f = open(trace_file, 'w+')
	f.write("0 r 0 0")
	f.close()

## Look for dramsim file and create a dummy if it not found
if( not os.path.exists(sys_ini) ):
	f = open(sys_ini, 'w+')
	f.write("DEBUG_POWER=False\nDEBUG_BUS=False\nDEBUG_BANKSTATE=False\nDEBUG_CMD_Q=False\nDEBUG_ADDR_MAP=False\nDEBUG_TRANS_Q=False\nDEBUG_BANKS=False\nEPOCH_LENGTH=100000\nROW_BUFFER_POLICY=open_page\nNUM_CHANS=1\nSCHEDULING_POLICY=rank_then_bank_round_robin\nCMD_QUEUE_DEPTH=32\nQUEUING_STRUCTURE=per_rank\nJEDEC_DATA_BUS_BITS=64\nADDRESS_MAPPING_SCHEME=scheme8\nTRANS_QUEUE_DEPTH=32\nTOTAL_ROW_ACCESSES=4\nPERMUTATION=0\n")
	f.close()

if( not os.path.exists(dev_ini) ):
	f = open(dev_ini, 'w+')
	f.write("tRFC=107\nNUM_BANKS=8\nBL=8\ntWTR=5\nNUM_COLS=2048\ntRTRS=1\nNUM_ROWS=32768\ntWR=10\ntRRD=4\nDEVICE_WIDTH=4\ntRTP=5\ntCMD=1\ntRCD=10\ntFAW=20\ntCKE=4\nCL=10\nAL=0\ntCK=1.5\ntRAS=24\nREFRESH_PERIOD=7800\ntCCD=4\nIDD2N=70\nIDD3N=90\nIDD3Pf=60\nIDD4R=230\nIDD4W=255\nIDD2P=10\nIDD1=130\nVdd=1.5\nIDD7=299\nIDD6=9\nIDD5=295\nIDD2Q=70\nIDD3Ps=60\nIDD6L=12\nIDD0=100\ntXP=4\ntRC=34\ntRP=10\n")
	f.close()

#########################
#		Configure		#
#########################
verbose = False
statFile = "stats.csv"
statLevel = 16


# Build Configuration Information
clock = "2660MHz"
max_reqs_cycle =  3
streamN = 500000

memory_clock = "200MHz"
memory_capacity = 2048  #MB

cache_link_latency = "300ps"


# Connect Cores & caches
print "Configuring CPU..."
cpu = sst.Component("cpu", "prospero.prosperoCPU")
cpu.addParams({
	'verbose': 				0,
	'cache_line_size':		64,
	'reader':				'prospero.ProsperoTextTraceReader',
	'pagesize':				4096,
    'clock':				clock,
	'max_outstanding':		16,
	'max_issue_per_cycle':	2,
    'readerParams.file':	trace_file,
})

print "Configuring Cache..."
l1cache = sst.Component("l1cache", "memHierarchy.Cache")
l1cache.addParams({
        "cache_frequency":          clock,
        "cache_size":               "32KiB",
        "associativity":            8,
        "access_latency_cycles":    4,
        "L1":                       1
})
l2cache = sst.Component("l2cache", "memHierarchy.Cache")
l2cache.addParams({
         "cache_frequency":          clock,
         "cache_size":               "2MiB",
         "associativity":            16,
         "access_latency_cycles":    6,        
	 "mshr_num_entries" :        16
 })

print "Configuring Memory"
memory = sst.Component("memory", "memHierarchy.MemController")
memory.addParams({
        "backend" : 			"memHierarchy.dramsim",
        "backend.device_ini" : 	dev_ini,
        "backend.system_ini" : 	sys_ini,
        "backend.mem_size" : 	memory_capacity,
        "clock" : 				memory_clock,
        "do_not_back" : 1
        })


cpu_l1_link = sst.Link("cpu_l1_link")
l1_l2_link = sst.Link("l1_l2_link")
l2_memory_link = sst.Link("l2_memory_link")

cpu_l1_link.connect((cpu, "cache_link", cache_link_latency), (l1cache, "high_network_0", cache_link_latency))

l1_l2_link.connect((l1cache, "low_network_0", cache_link_latency), (l2cache, "high_network_0", cache_link_latency))

l2_memory_link.connect((l2cache, "low_network_0", cache_link_latency), (memory, "direct_link", cache_link_latency))


# ===============================================================================

# Enable SST Statistics Outputs for this simulation
sst.setStatisticLoadLevel(statLevel)
sst.enableAllStatisticsForAllComponents({"type":"sst.AccumulatorStatistic"})

sst.setStatisticOutput("sst.statOutputCSV")
sst.setStatisticOutputOptions( {
    "filepath"  : statFile,
    "separator" : ", "
    } )

print "Completed configuring the EX1 model"
